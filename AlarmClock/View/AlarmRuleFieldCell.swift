//
//  AlarmRuleFieldCell.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 22/07/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

protocol AlarmRuleDataChangedDelegate: class {
    func alarmDataChanged()
}

class AlarmRuleFieldCell : UITableViewCell {
    
    var delegate: AlarmRuleDataChangedDelegate?
    
    fileprivate var rule: AlarmRule!
    
    func update(with rule:AlarmRule) {
        self.rule = rule
        updateUI()
    }
    
    fileprivate func updateUI() {
        assertionFailure("this is abstract class")
    }
    
    @IBAction func actionNeeded() {
        assertionFailure("this is abstract class")
    }

    @IBAction func valueChanged() {
        assertionFailure("this is abstract class")
    }
}

class AlarmRuleTimeCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var timePicker: UIDatePicker!
    
    override func updateUI() {
        let d = Date().dateWith(hour: rule.time.hour, minutes: rule.time.minutes, seconds: rule.time.seconds)!
        timePicker.date = d
    }
    
    override func valueChanged() {
        let d = timePicker.date
        rule.time.hour = d.hour
        rule.time.minutes = d.minute
        rule.time.seconds = d.second
        delegate?.alarmDataChanged()
    }
}


class AlarmRuleReminderCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var typeSegemnt: UISegmentedControl!
    
    override func updateUI() {
        typeSegemnt.selectedSegmentIndex = rule.isReminder.int
    }
    
    override func valueChanged() {
        rule.isReminder = typeSegemnt.selectedSegmentIndex == 1
        delegate?.alarmDataChanged()
        
        if DataManager.shared.shouldPresentReminderTutorial {
            UIAlertController.show(message: "When selecting 'Reminder' option, The alarm will behave as simple Notification with default short Notification sound", buttons: ["OK"], cancel: nil, completion: nil)
        }
    }
}

class AlarmRuleTitleCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    
    override func updateUI() {
        lblTitle.text = rule.title.isEmpty ? "None" : rule.title
    }

    override func actionNeeded() {
        TextPickerView.showWithTitle("Alarm Title", text: rule.title, placeholder: "Alarm Title") { [weak self] (txt) in
            if let txt = txt {
                self?.rule.title = txt
                self?.updateUI()
            }            
        }
    }
}


class AlarmRuleVacationCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var switchVacation : UISwitch!
    
    override func updateUI() {
        switchVacation.isOn = self.rule.onVacation
    }
    
    override func valueChanged() {
        
        if SettingsTableViewController.vacationCount == 0 {
            
            let title = "You haven't set any vacation yet"
            let message = "If you switch this off, the alarm won't work on vacations.\nDo you want to set vacations now? You can always change those through the settings screen."
            let buttons = ["Set vacations"]
            let cancel = "Later"
            UIAlertController.show(title: title, message: message, buttons: buttons, cancel: cancel) {
                if $0 != cancel {
                    UIWindow.presentedViewController?.performSegue(withIdentifier: "vacationsFromAlarm", sender: nil)
                }
            }
        }
        
        self.rule.onVacation = switchVacation.isOn
    }
}

class AlarmRuleSafetyCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var switchSafety : UISwitch!
    
    override func updateUI() {
        switchSafety.isOn = self.rule.awakeSafety
    }
    
    override func valueChanged() {
        self.rule.awakeSafety = switchSafety.isOn
        if DataManager.shared.shouldPresentSafetyTutorial {
            let tryNow = "Try it"
            UIAlertController.show(message: "When awake safety is activated, it will validate your wakefulness by presenting a simple math question that you will need to answer.", buttons: [tryNow], cancel: "OK") {
                
                if $0 == tryNow {
                    DispatchQueue.main.asyncAfter(deadline: 0.5.delay) {
                        DataManager.shared.validateAwakefulness(completion:nil)
                    }
                }
                
            }
        }
    }
}


class AlarmRuleDurationCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var stepperDuration : UIStepper!
    @IBOutlet weak var lblValue : UILabel!
    
    override func updateUI() {
        stepperDuration.value = self.rule.duration
        lblValue.text = "\(self.rule.duration.int) sec"

    }
    
    override func valueChanged() {
        self.rule.duration = stepperDuration.value
        updateUI()
    }
}

class AlarmRuleSnoozeDelayCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var stepperDelay : UIStepper!
    @IBOutlet weak var lblValue : UILabel!
    
    override func updateUI() {
        stepperDelay.value = self.rule.snoozeDelay.toMinutes
        lblValue.text = "\(self.rule.snoozeDelay.toMinutes.int) min"
    }
    
    override func valueChanged() {
        self.rule.snoozeDelay = stepperDelay.value.minutes
        updateUI()
    }
}


class AlarmRuleSnoozeCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var stepperSnooze : UIStepper!
    @IBOutlet weak var lblValue : UILabel!
    
    override func updateUI() {
        stepperSnooze.value = self.rule.snoozeNumber.double
        let times = self.rule.snoozeNumber == 1 ? "time" : "times"
        lblValue.text = self.rule.snoozeNumber == 0 ? "None" : "\(self.rule.snoozeNumber) \(times)"
    }
    
    override func valueChanged() {
        self.rule.snoozeNumber = stepperSnooze.value.int
        updateUI()
    }
}

class AlarmRuleDateCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var lblValue : UILabel!
    
    override func updateUI() {
        lblValue.text = self.rule.noneRepeat.exist() ? self.rule.repeatOption.text : "Repeating"
    }
}

class AlarmRuleRepeatCell: AlarmRuleFieldCell {
    
    @IBOutlet weak var lblValue : UILabel!
    
    override func updateUI() {
        lblValue.text = self.rule.noneRepeat.notExist() ? self.rule.repeatOption.text : "None"
    }
}

