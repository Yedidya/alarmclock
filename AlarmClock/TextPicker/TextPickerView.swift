//
//  TextPickerView.swift
//  MyTravel
//
//  Created by Yedidya Reiss on 20/12/2016.
//  Copyright © 2016 Quickode. All rights reserved.
//

import UIKit


class TextPickerView: UIView, UITextViewDelegate {

    @IBOutlet weak var txt: UITextView!
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var lblCharsAmount: UILabel?
    @IBOutlet weak var viewWrapper : UIView!

    @IBOutlet weak var lblPlaceholder : UILabel!

    /** 0 if there is no limitation */
    var maxChars = 0

    private var completion : ((_ text:String?)->Void)?
    
    @discardableResult class func showWithTitle(_ title:String, text:String? = nil, placeholder:String? = nil, maxChars : Int? = nil, completion: ((_ text:String?)->Void)?) -> TextPickerView? {
        
        guard let picker = Bundle.main.loadNibNamed("TextPickerView", owner: nil, options: nil)?.first as? TextPickerView else {
            return nil
        }
        picker.maxChars = maxChars ?? 0
        picker.completion = completion
        picker.lblTitle?.text = title
        picker.txt.text = text
        picker.lblPlaceholder.text = placeholder
        picker.frame = UIApplication.shared.keyWindow!.frame
        picker.updatePlaceholder()
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.addSubview(picker)
            picker.alpha = 0
            UIView.animate(withDuration: 0.2) {
                picker.alpha = 1
            }
        }
        
        return picker
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewWrapper.layer.cornerRadius = 8
        updateAmountLabel()
        txt.becomeFirstResponder()
        updatePlaceholder()
    }
    
    
    
    
    fileprivate func updateAmountLabel() {
        let count = txt.text?.count ?? 0
        lblCharsAmount?.text = String(maxChars - count)
        lblCharsAmount?.isHidden = maxChars <= 0
        
    }
    

    private func updatePlaceholder() {
        lblPlaceholder.isHidden = txt.text.count > 0
    }
    
    private var animating = false
    private func limitError() {
        guard !animating else { return }
        animating = true
        self.lblCharsAmount?.textColor = .red
        UIView.animate(withDuration: 0.2, animations: {
            self.lblCharsAmount?.transform = CGAffineTransform(scaleX: 1.8, y: 1.8)
        }) { (done) in
            UIView.animate(withDuration: 0.3, delay: 1, options: .curveEaseOut, animations: {
                self.lblCharsAmount?.transform = CGAffineTransform(scaleX: 1, y: 1)
            }) { (done) in
                self.lblCharsAmount?.textColor = .lightGray
                self.animating = false
            }
        }
    }
    
    //MARK: text view delegate
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        var txtAfterUpdate:NSString = txt.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: text) as NSString
        let numberOfChars = txtAfterUpdate.length
        
        let legal = maxChars <= 0 || numberOfChars <= maxChars
        
        if !legal {
            limitError()
        }
        
        return legal
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        updatePlaceholder()
        updateAmountLabel()
    }
    
    
    func close() {
        self.endEditing(true)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
        
    }
    
    
    @IBAction func donePressed() {
        completion?(txt.text)
        close()
    }
    
    
    @IBAction func cancelPressed() {
        completion?(nil)
        close()
    }
    

}
