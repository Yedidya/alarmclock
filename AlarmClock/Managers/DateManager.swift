//
//  DateManager.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 30/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import Foundation

class DateManager {
    
    static let repeatDateFormat: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "MMM dd, EEEE"
        return f
    }()
    
    static let specificDateFormat: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "MMM dd, YYYY, EEEE"
        return f
    }()

}
