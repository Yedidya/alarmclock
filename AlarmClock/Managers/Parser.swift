//
//  Parser.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 03/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import Foundation

extension DayTime: JSONSerializable {
    var jsonObject: JSONObject? {
        return ["hour":hour,"minutes":minutes,"seconds":seconds]
    }
}
extension JSON {
    var dayTime: DayTime? {
        guard let h = self["hour"].int, let m = self["minutes"].int, let s = self["seconds"].int else { return nil }
        return DayTime(hour: h, minutes: m, seconds: s)
    }
}


extension NoneRepeat: JSONSerializable {
    var jsonObject: JSONObject? {
        return ["date":date].jsonObject
    }
}
extension JSON {
    var noneRepeat: NoneRepeat? {
        guard let d = self["date"].date else { return nil }
        let n = NoneRepeat()
        n.date = d
        return n
    }
}

extension WeeklyRepeat: JSONSerializable {
    var jsonObject: JSONObject? {
        return ["days":days].jsonObject
    }
}
extension JSON {
    var weeklyRepeat: WeeklyRepeat? {
        guard let d = self["days"].array else { return nil }
        let r = WeeklyRepeat()
        r.days = d.map { $0.boolValue }
        return r
    }
}


extension MonthlyRepeat: JSONSerializable {
    var jsonObject: JSONObject? {
        return ["days":days].jsonObject
    }
}
extension JSON {
    var monthlyRepeat: MonthlyRepeat? {
        guard let d = self["days"].array else { return nil }
        let r = MonthlyRepeat()
        r.days = d.map { $0.boolValue }
        return r
    }
}



extension YearlyRepeat: JSONSerializable {
    var jsonObject: JSONObject? {
        return ["month":month,"day":day]
    }
}
extension JSON {
    var yearlyRepeat: YearlyRepeat? {
        guard let d = self["day"].int, let m = self["month"].int else { return nil }
        let r = YearlyRepeat()
        r.day = d
        r.month = m
        return r
    }
}


extension RepeatOption: JSONSerializable {
    var jsonObject: JSONObject? {
        switch self {
        case .none(let r): return ["type":"none","repeat":r].jsonObject
        case .weekly(let r): return ["type":"weekly","repeat":r].jsonObject
        case .monthly(let r): return ["type":"monthly","repeat":r].jsonObject
        case .yearly(let r): return ["type":"yearly","repeat":r].jsonObject
        }
    }
}
extension JSON {
    var repeatOption: RepeatOption? {
        guard let t = self["type"].string else { return nil }
        let r = self["repeat"]
        
        switch t {
        case "none"     : return .none(date: r.noneRepeat!)
        case "weekly"   : return .weekly(repeat:r.weeklyRepeat!)
        case "monthly"  : return .monthly(repeat:r.monthlyRepeat!)
        case "yearly"   : return .yearly(repeat:r.yearlyRepeat!)
        default: return nil
        }
    }
}

extension AlarmRule: JSONSerializable {
    var jsonObject: JSONObject? {
        var json: JSONDictionary = [:]
        json["id"] = id
        json["time"] = time
        json["title"] = title
        json["disabled"] = disabled
        json["snoozeNumber"] = snoozeNumber
        json["onVacation"] = onVacation
        json["duration"] = duration
        json["snoozeDelay"] = snoozeDelay
        json["awakeSafety"] = awakeSafety
        json["repeatOption"] = repeatOption
        json["isReminder"] = isReminder
        json["startDate"] = startDate
        json["endDate"] = endDate
        return json.jsonObject
    }
}
extension JSON {
    var alarmRule: AlarmRule? {
        let id = self["id"].string!
        let a = AlarmRule(id: id)
        a.time = self["time"].dayTime!
        a.title = self["title"].string!
        a.disabled = self["disabled"].bool!
        a.snoozeNumber = self["snoozeNumber"].int!
        a.onVacation = self["onVacation"].bool!
        a.duration = self["duration"].double!
        a.snoozeDelay = self["snoozeDelay"].double!
        a.awakeSafety = self["awakeSafety"].bool!
        a.repeatOption = self["repeatOption"].repeatOption!
        a.isReminder = self["isReminder"].bool ?? false
        a.startDate = self["startDate"].date
        a.endDate = self["endDate"].date
        return a
    }
}

extension AlarmRule {
    func copy() -> AlarmRule {
        return JSON(with: self).alarmRule!
    }
}



extension Alarm: JSONSerializable {
    var jsonObject: JSONObject? {
        var json: JSONDictionary = [:]
        json["ruleId"] = ruleId
        json["originalDate"] = originalDate
        json["time"] = time
        json["title"] = title
        json["disabled"] = disabled
        json["date"] = date
        json["snoozeDelay"] = snoozeDelay
        json["onVacation"] = onVacation
        json["edited"] = edited
        json["done"] = done
        json["missedDelay"] = missedDelay
        json["snoozeDuration"] = snoozeDuration
        json["snoozeCount"] = snoozeCount
        json["isInVacation"] = isInVacation
        json["onVacation"] = onVacation
        return json.jsonObject
    }
}
extension JSON {
    var alarm: Alarm? {
        
        let rId = self["ruleId"].string!
        let alarm = Alarm(ruleId: rId)
        
        alarm.time = self["time"].dayTime!
        alarm.originalDate = self["originalDate"].date!
        alarm.title = self["title"].string!
        alarm.disabled = self["disabled"].bool!
        alarm.date = self["date"].date!
        alarm.snoozeDelay = self["snoozeDelay"].double!
        alarm.onVacation = self["onVacation"].bool!
        alarm.edited = self["edited"].bool ?? false
        alarm.done = self["done"].bool ?? false
        alarm.missedDelay = self["missedDelay"].double ?? 30.seconds
        alarm.snoozeDuration = self["snoozeDuration"].double ?? 10.minutes
        alarm.snoozeCount = self["snoozeCount"].int ?? 0
        alarm.isInVacation = self["isInVacation"].bool ?? false
        alarm.onVacation = self["onVacation"].bool ?? true
        return alarm
    }
}



extension Vacation {
    func copy() -> Vacation {
        return JSON(with: self).vacation!
    }
}


extension Vacation: JSONSerializable {
    var jsonObject: JSONObject? {
        var json: JSONDictionary = [:]
        json["id"] = id
        json["start"] = start
        json["end"] = end
        json["title"] = title
        json["enabled"] = enabled
        json["yearly"] = yearly
        return json.jsonObject
    }
}

extension JSON {
    var vacation: Vacation? {
        let v = Vacation()
        v.id = self["id"].string!
        v.start = self["start"].date!
        v.end = self["end"].date!
        v.title = self["title"].string!
        v.enabled = self["enabled"].bool!
        v.yearly = self["yearly"].bool!
        return v
    }
}

