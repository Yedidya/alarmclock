//
//  ConnectionManager.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 09/09/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import Foundation
import FirebaseDatabase


class Config {
    var lastIosVersion: String
    var mandatoryIosVersion: String
    var iosUrl: String = "http://appstore.com/yedidyareiss"
    var testForIosPushAuth = false {
        didSet { NotificationsAuthViewController.validateAuth() }
    }
    
    init(lastIosVersion:String, mandatoryIosVersion:String) {
        self.mandatoryIosVersion = mandatoryIosVersion
        self.lastIosVersion = lastIosVersion
    }
}


class ConnectionManager {

    static let shared = ConnectionManager()

    private let db = Database.database().reference()

    /// call on app launch - will activate the constructor
    func setup() {}
    
    private init() {
        automaticFetch()
    }
    
    private func automaticFetch() {
        db.child("Config").observe(.value) {
            let res = $0.value as! [String:Any]
            let config = Config(lastIosVersion: res["lastIosVersion"] as! String, mandatoryIosVersion: res["mandatoryIosVersion"] as! String)
            config.iosUrl = res["iosUrl"] as! String
            config.testForIosPushAuth = res["testForIosPushAuth"] as! Bool
            DataManager.shared.config = config
        }
    }
    
     
    func updateUserData() {
        let data: [String:Any] = [
            "general": ["osVersion": UIDevice.current.systemVersion,"appVersion":Bundle.appVersion(),"type":"ios"],
            "fcmToken": DataManager.shared.fcmToken,
            "data": [
                "weeklyVaction": DataManager.shared.weeklyVacation.jsonObject!,
                "arrVacations": DataManager.shared.arrVacations.jsonObject!,
                "arrAlarmRules": DataManager.shared.arrAlarmRules.jsonObject!,
                "arrAlarms": DataManager.shared.arrAlarms.jsonObject!
            ]
        ]
        
        db.child("Devices").child(DataManager.shared.deviceId).setValue(data)
    }
}
