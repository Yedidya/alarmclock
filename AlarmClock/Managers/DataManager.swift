//
//  DataManager.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 27/07/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit
import UserNotifications
import AVFoundation



class DataManager {
    static let shared = DataManager()
    
    var config: Config {
        didSet { checkForMandatoryVersion() }
    }
    
    var deviceId: String {
        if let uuid = UserDefaults.standard.string(forKey: "alarmClock_deviceId") {
            return uuid
        }
        let newUuid = UUID().uuidString
        UserDefaults.standard.set(newUuid, forKey: "alarmClock_deviceId")
        return newUuid
    }
    
    var fcmToken: String = "" {
        didSet { ConnectionManager.shared.updateUserData() }
    }

    
    private(set) var weeklyVacation = WeeklyRepeat()
    private(set) var arrVacations: [Vacation] = []
    private(set) var arrAlarmRules: [AlarmRule] = []
    private(set) var arrAlarms: [Alarm] = []
    
    private init() {
        
        config = Config(lastIosVersion: Bundle.appVersion(), mandatoryIosVersion: Bundle.appVersion())
        
        weeklyVacation.clear()
        loadData()
        updateData()
    }
    
    var shouldPresentSafetyTutorial: Bool {
        set { UserDefaults.standard.set(newValue, forKey: "shouldPresentSafetyTutorial") }
        get {
            let val = UserDefaults.standard.value(forKey: "shouldPresentSafetyTutorial") as? Bool ?? true
            UserDefaults.standard.set(false, forKey: "shouldPresentSafetyTutorial")
            return val
        }
    }

    var shouldPresentReminderTutorial: Bool {
        set { UserDefaults.standard.set(newValue, forKey: "shouldPresentReminderTutorial") }
        get {
            let val = UserDefaults.standard.value(forKey: "shouldPresentReminderTutorial") as? Bool ?? true
            UserDefaults.standard.set(false, forKey: "shouldPresentReminderTutorial")
            return val
        }
    }

    
    private func checkForMandatoryVersion() {
        if config.mandatoryIosVersion.isNewerVersion(than: Bundle.appVersion()) {
            let errorMessage = "There is a newer version that fixed some critical issue in the app, please update the app in order to continue"
            UIAlertController.show(title: "Error", message: errorMessage, buttons: ["OK"], cancel: nil) { _ in
                let url = URL(string:self.config.iosUrl)!
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    private func loadData() {

        log.d("loading data")
        let ud = UserDefaults.standard
        if let rules = ud.string(forKey: "kLocalRules"), let alarms = ud.string(forKey: "kLocalAlarms") {
            arrAlarmRules = JSON(fromJsonString: rules).array!.map { $0.alarmRule! }
            arrAlarms = JSON(fromJsonString: alarms).array!.map { $0.alarm! }
        }
        
        if let vacations = ud.string(forKey: "kLocalVacations"), let weekly = ud.string(forKey: "kLocalWeeklyVacation") {
            arrVacations = JSON(fromJsonString: vacations).array!.map { $0.vacation! }
            weeklyVacation = JSON(fromJsonString: weekly).weeklyRepeat!
        }
    }
    
    private func saveData() {

        log.d("saving data")
        let rules = JSON(with: arrAlarmRules).jsonString!
        UserDefaults.standard.set(rules, forKey: "kLocalRules")
        
        let alarms = JSON(with: arrAlarms).jsonString!
        UserDefaults.standard.set(alarms, forKey: "kLocalAlarms")
        
        let vacations = JSON(with: arrVacations).jsonString!
        UserDefaults.standard.set(vacations, forKey: "kLocalVacations")
        
        let weekly = JSON(with: weeklyVacation).jsonString!
        UserDefaults.standard.set(weekly, forKey: "kLocalWeeklyVacation")        
    }
    
    
    func updateData() {
        log.d("updating data")
        updateRules()
        updateAlarmsData()
        saveData()
        updateNotifications()
        checkForMandatoryVersion()
    }
    
    
    private func updateRules() {
        log.d("updating rules")
        arrAlarmRules = arrAlarmRules.sorted { $0.time.intervals }
        
        let now = Date()
        arrAlarmRules.forEach {
            if let d = $0.noneRepeat?.date, d + 1.dayComponent < now {
                $0.disabled = true
            }
        }
    }
    
    private func updateAlarmsData() {
        log.d("updating alarms")
        
        let today = Date().start(of: .day)!
        
        var alarms = arrAlarms.filter { $0.missedDate > today }
        
        var map = alarms.dictionary { ($0.id,true) }
        
        var new: [Alarm] = []
        let nextCount = 5
        arrAlarmRules.forEach {
            let newAlarms = $0.alarms(max: nextCount)
            new.append(contentsOf: newAlarms)
        }
        
        alarms += new.filter { map[$0.id].notExist() }
        
        arrAlarms = alarms.sorted { $0.actualAlarmDate }
        
        arrAlarms.forEach { (a) in
            let weekly = weeklyVacation.days[a.actualAlarmDate.weekDay-1]
            let v = arrVacations.exist { $0.enabled && $0.contained(date: a.actualAlarmDate) }
            a.isInVacation = v || weekly
        }
    }
    
    
    private func updateNotifications() {
        log.d("updating notifications")
        
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        var count = 0
        let maxNotifications = 62
        var lastDate = Date()
        arrAlarms.forEach {
            count += notificationsCount(for: $0)
            if count <= maxNotifications {
                setNotifications(alarm: $0)
                print("notifications until now - \(count)")
                lastDate = $0.missedDate
            }
        }
        
        if count > maxNotifications / 2 {
            setNotificationsUpdate(date: lastDate + 5.minutes)
        }
    }
    
    
    private func notificationsCount(for alarm: Alarm) -> Int {
        let alarmRule = rule(for:alarm)

        if alarmRule.isReminder { return 1 }
        
        guard alarm.shouldWork, !alarm.done, alarm.missedDate.timeIntervalSinceNow > 0 else {
            return 0
        }
        
        let durationStep = 30.seconds
        let count = alarmRule.duration / durationStep
        
        return (alarm.snoozeCount + 1) * count.int
    }
    
    
    private func setNotifications(alarm:Alarm, snooze:Int = 0) {
        
        let alarmRule = rule(for:alarm)
        
        if snooze > alarm.snoozeCount { return }
        if snooze > 0 && alarmRule.isReminder { return }
        
        setNotifications(alarm: alarm, snooze: snooze + 1)
        
        let d = alarm.actualAlarmDate
        let delay = d.timeIntervalSinceNow + snooze.double * alarm.snoozeDuration
        
        
        guard delay >= 0, alarm.shouldWork, !alarm.done else {
            return
        }
        
        let durationStep = 30.seconds
        let count = alarmRule.isReminder ? 1 : alarmRule.duration / durationStep
        
        Array(0..<count.int).forEach {
            
            let content = UNMutableNotificationContent()
            content.title = "\(alarm.time.hour):\(alarm.time.minutes.string2d) Alarm"
            content.body = alarm.title
            content.sound = alarmRule.isReminder ? .default() : UNNotificationSound(named: "iphone6.mp3")
            content.categoryIdentifier = notificationCategory(rule: alarmRule).rawValue
            content.userInfo = ["alarmId":alarm.id]
            
            let time = delay + $0.double * durationStep
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: time, repeats: false)
            
            let notificationId = alarm.id + "_#\($0)_Sn#\(snooze)"
            let request = UNNotificationRequest(identifier: notificationId , content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request)
            let date = Date() + delay
            //TODO: test performance of log
            log.v("notification was set: " + notificationId)
            log.v("notification date:\(date.day)-\(date.month)-\(date.year), time: \(date.hour):\(date.minute.string2d), title: \(alarm.title)")
        }
    }
    
    
    private func setNotificationsUpdate(date:Date) {
        log.i("set update notification for date: \(date)")
        let content = UNMutableNotificationContent()
        content.title = "Alarms Data Update"
        content.body = "You didn't enter the alarm app for a while. Do you still want receive alarm notifications?"
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = Notification.Category.update.rawValue
        

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: date.timeIntervalSinceNow, repeats: false)
        
        let notificationId = "alarmUpdateReminderId"
        let request = UNNotificationRequest(identifier: notificationId , content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request)
    }
    
    
    private func notificationCategory(rule:AlarmRule) -> Notification.Category {
        if rule.isReminder {
            return .stop
        }
        if rule.snoozeNumber == 0 {
            return rule.awakeSafety ? .safety : .stop
        }
        return rule.awakeSafety ? .snooze_safety : .snooze_stop
    }
    
    private func rule(for alarm: Alarm) -> AlarmRule {
        return arrAlarmRules.first { $0.id == alarm.ruleId }!
    }
    
    
    func addVacation(vacation: Vacation) {
        log.d("adding vacation - title: \(vacation.title), id: \(vacation.id)")
        arrVacations.append(vacation)
        updateData()
    }
    
    func deleteVacation(vacation: Vacation) {
        log.d("delete vacation - title: \(vacation.title), id: \(vacation.id)")
        arrVacations = arrVacations.filter { $0.id != vacation.id }
        updateData()
    }
    
    func vacationUpdated(vacation: Vacation) {
        log.d("update vacation - title: \(vacation.title), id: \(vacation.id)")
        let i = arrVacations.index { $0.id == vacation.id }!
        arrVacations[i] = vacation
        updateData()
    }
    
    func addRule(rule: AlarmRule) {
        arrAlarmRules.append(rule)
        updateData()
    }
    
    func deleteRule(rule: AlarmRule, update: RuleEditing = .affectAll) {
        log.d("delete rule - title: \(rule.title), ruleId: \(rule.id)")

        arrAlarmRules = arrAlarmRules.filter { $0.id != rule.id }
        
        let edited = arrAlarms.filter { $0.ruleId == rule.id && $0.edited }
        
        arrAlarms = arrAlarms.filter { $0.ruleId != rule.id }
        
        if update == .affectUnEdited {
            arrAlarms += edited
        }
        
        updateData()
    }
    
    func ruleUpdated(rule: AlarmRule, update: RuleEditing = .affectAll) {
        log.d("update rule - title: \(rule.title), ruleId: \(rule.id)")

        let i = arrAlarmRules.index { $0.id == rule.id }!
        arrAlarmRules[i] = rule
        
        let edited = arrAlarms.filter { $0.ruleId == rule.id && $0.edited }
        let new = rule.alarms(max: 5)
        let delta = new.remove(array: edited) { $0.id }
        
        let append = update == .affectUnEdited ? edited + delta : new

        arrAlarms = arrAlarms.filter { $0.ruleId != rule.id } + append
        
        updateData()
    }
    
    func alarmUpdated(alarm: Alarm) {
        let i = arrAlarms.index { $0.id == alarm.id }!
        arrAlarms[i] = alarm
        updateData()
    }
    
    func resetAlarm(alarm: Alarm) {
        let i = arrAlarms.index { $0.id == alarm.id }!
        arrAlarms.remove(at: i)
        updateData()
    }
    
    func handleNotifications(alarmId: String, action: Notification.Action) {
        
        guard let alarm = arrAlarms.first(where: { $0.id == alarmId }) else {
            log.e("does not exist! alarm with id: \(alarmId)")
            return
        }
        
        log.i("handel notification for alarm title: \(alarm.title)")

        if action == .snooze {
            alarm.snoozeDelay += alarm.snoozeDuration
            alarmUpdated(alarm: alarm)
        }
        if action == .stop {
            
            let done = { [weak self] in
                alarm.done = true
                self?.alarmUpdated(alarm: alarm)
            }
            
            if rule(for: alarm).awakeSafety {
                validateAwakefulness { done() }
            } else {
                done()
            }
            
        }
    }
    
    
    func validateAwakefulness(completion:(()->Void)?) {
        if let _ = UIWindow.presentedViewController {
            playSound()
            AwakeSafetyViewController.show { (done) in
                self.player?.stop()
                completion?()
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: 0.5.delay) { [weak self] in
                self?.validateAwakefulness(completion: completion)
            }
        }
    }
    
    private var player: AVAudioPlayer?
    
    private func playSound() {
        guard let url = Bundle.main.url(forResource: "iphone6", withExtension: "mp3") else { return }
        //TOOD: variable volume
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            player?.numberOfLoops = 10
            player?.prepareToPlay()
            player?.play()
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    func setupNotificationsActions() {
        let snooze = UNNotificationAction(identifier: Notification.Action.snooze.rawValue, title: Notification.Action.snooze.rawValue, options: [])
        let stop = UNNotificationAction(identifier: Notification.Action.stop.rawValue, title: Notification.Action.stop.rawValue, options: [])
        let stopSafety = UNNotificationAction(identifier: Notification.Action.stop.rawValue, title: Notification.Action.stop.rawValue, options:[.foreground])
        
        
        let stopC = UNNotificationCategory(identifier: Notification.Category.stop.rawValue, actions: [stop], intentIdentifiers: [], options: [])
        let safetyC = UNNotificationCategory(identifier: Notification.Category.safety.rawValue, actions: [stopSafety], intentIdentifiers: [], options: [])
        let snoozeC = UNNotificationCategory(identifier: Notification.Category.snooze_stop.rawValue, actions: [snooze,stop], intentIdentifiers: [], options: [])
        let snoozeSafetyC = UNNotificationCategory(identifier: Notification.Category.snooze_safety.rawValue, actions: [snooze,stopSafety], intentIdentifiers: [], options: [])
        
        
        let update = UNNotificationAction(identifier: Notification.Action.update.rawValue, title: "Continue", options:[])
        
        let updateC = UNNotificationCategory(identifier: Notification.Category.update.rawValue, actions: [update], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([stopC,safetyC,snoozeC,snoozeSafetyC,updateC])

    }
}



enum RuleEditing {
    case affectAll
    case affectUnEdited
}


struct Notification {
    enum Action: String {
        case snooze = "Snooze"
        case stop = "Stop"
        case update = "Update"
    }
    
    enum Category: String {
        case stop = "stop"
        case safety = "safety"
        case snooze_stop = "snooze&stop"
        case snooze_safety = "snooze&safety"
        case update = "update"
    }
}




