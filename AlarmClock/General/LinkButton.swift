//
//  LinkButton.swift
//  Yedidya's tests
//
//  Created by Yedidya Reiss on 04/02/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class LinkButton: UIButton {
    @IBInspectable var link: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(openLink), for: .touchUpInside)
    }
    
    @objc private func openLink() {
        guard let link = link, let url = URL(string: link) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
