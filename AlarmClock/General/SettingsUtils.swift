//
//  SettingsUtils.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 30/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import Foundation

import MessageUI

class SettingsUtils: NSObject, MFMailComposeViewControllerDelegate {
    
    private static let shared = SettingsUtils()
    
    
    /////////////////////////////////////////////////
    //////////////////    mail    ///////////////////
    /////////////////////////////////////////////////
    // MARK: mail -
    
    static func contactUs(to email:String, subject:String, body:String = "") {
        if MFMailComposeViewController.canSendMail() {
            let vc = MFMailComposeViewController()
            
            vc.setToRecipients([email])
            
            vc.setSubject(subject)
            vc.setMessageBody(body, isHTML: false)
            vc.mailComposeDelegate = SettingsUtils.shared;
            UIWindow.presentedViewController?.present(vc, animated: true, completion: nil)
        } else {
            UIAlertController.show(title: "Error", message: "It seems that the default email isn't configured on your device.", buttons: [], cancel: "Cancel", completion: nil)
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    ////////////////////////////////////////
    //////////////// share /////////////////
    ////////////////////////////////////////
    // MARK: - share -
    
    
    static func share(text: String, share: String = "Copy & Share", cancel: String = "Cancel") {
        
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        
        let shA = UIAlertAction(title: share, style: .default) { (action) -> Void in
            
            UIPasteboard.general.string = text
            
            let imageToShare = UIImage(named:"icon")
            let itemsToShare : [Any] = [text, imageToShare!]
            
            let activityVC = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
            
            activityVC.excludedActivityTypes = [UIActivityType.print, UIActivityType.copyToPasteboard, UIActivityType.assignToContact, UIActivityType.saveToCameraRoll];
            UIWindow.presentedViewController?.present(activityVC, animated: true, completion: nil)
        }
        
        let cA = UIAlertAction(title: cancel, style: .cancel, handler: nil)
        
        alert.addAction(shA)
        alert.addAction(cA)
        
        UIWindow.presentedViewController?.present(alert, animated: true, completion: nil)
        
    }
}

