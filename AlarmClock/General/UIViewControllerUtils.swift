//
//  UIViewControllerUtils.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 30/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit


extension UIViewController {
    
    @IBAction func returnAsPopOrDismiss() {
        if let c = self.navigationController?.viewControllers.count , c > 1 {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}



extension UIViewController {
    func blinkMessage(text:String) {
        let l = UILabel(frame: CGRect(x: 0, y: 0, width: 280, height: 1000))
        l.numberOfLines = 0
        l.text = text
        l.sizeToFit()
        l.textColor = UIColor.white
        l.textAlignment = .center
        
        let v = UIView()
        v.width = l.width + 30
        v.height = l.height + 10
        v.setBaseBorder()
        v.layer.cornerRadius = min(20, v.height/2)
        v.backgroundColor = UIColor.darkGray.withAlphaComponent(0.8)
        
        v.addSubview(l)
        l.center = (v.frame.size/2).point
        
        v.topCenter.y = 60
        v.topCenter.x = self.view.width/2
        
        v.alpha = 0
        
        self.view.addSubview(v)
        
        UIView.animate(withDuration: 0.3) {
            v.alpha = 1
        }
        
        UIView.animate(withDuration: 0.2, delay: 2.2, options: .curveLinear, animations: {
            v.alpha = 0
        }) { (done) in
            v.removeFromSuperview()
        }
    }
}


