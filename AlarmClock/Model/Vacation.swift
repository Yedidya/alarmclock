//
//  Vacation.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 26/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import Foundation

class Vacation {
    
    var id = UUID().uuidString
    
    var title = ""
    var start: Date!
    var end: Date!
    var enabled = true
    var yearly = true
//    var calendar = Calendar.current
    
    func contained(date:Date) -> Bool {
        if start == nil || end == nil { return false }
        return date < end && date > start
    }    
}


extension Vacation {
    func dateString(date:Date) -> String {
        let formatter = yearly ? DateManager.repeatDateFormat : DateManager.specificDateFormat
        var d = date
        if yearly {
            let today = Date()
            d = Date(year: today.year, month: d.month, day: d.day, hour: d.hour, minutes: d.minute, seconds: d.second)!
        }
        return formatter.string(from: d)
    }
}
