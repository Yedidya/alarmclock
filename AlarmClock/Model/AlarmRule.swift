//
//  AlarmRule.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 08/07/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import Foundation





class AlarmRule {
    
    let id : String
    
    var time = DayTime()
    var title = ""
    var disabled = false
    
    var snoozeNumber = 0
    var onVacation = true
    var duration: TimeInterval = 60.seconds
    var snoozeDelay: TimeInterval = 10.minutes
    var awakeSafety = false
    
    var isReminder = false //shourt default sound
    
    var repeatOption: RepeatOption = .none(date: NoneRepeat())
    
    var startDate: Date?
    var endDate: Date?
    
    init(id:String? = nil) {
        self.id = id ?? UUID().uuidString
    }
    
    func alarms(max count: Int) -> [Alarm] {
        var alarms = [Alarm]()

        var d = Date()
        if let st = startDate, st > Date() {
            d = st
        }
        
        for _ in 0..<count {
            guard let next = repeatOption.next(from: d) else { break }
            if let end = endDate, end < next { break }
            
            let a = alarm()
            a.date = next
            alarms.append(a)
            d = next + 1.dayComponent
        }
        return alarms
    }
    
    private func alarm() -> Alarm {
        let alarm = Alarm(ruleId:id)
        alarm.time = time
        alarm.title = title
        alarm.disabled = disabled
        alarm.onVacation = onVacation
        alarm.missedDelay = snoozeNumber.double * snoozeDelay + duration
        alarm.snoozeDuration = snoozeDelay
        alarm.snoozeCount = snoozeNumber
        return alarm
    }
}


class Alarm {
    var ruleId: String!
    
    var id: String {
        return ruleId + "-" + originalDate.description
    }
    
    
    var time = DayTime()
    var title = ""
    var disabled = false

    var isInVacation = false
    
    var done = false
    
    /// the date that was set from the alarm rule, represent the alarm day
    var originalDate: Date!
    
    /// the date that represent the actual day of the alarm
    var date: Date! { didSet { originalDate = originalDate ?? date } }
    
    /// return the day with the time of the alert
    var fullDate: Date { return date.start(of: .day)! + time.intervals.seconds }
    
    /// return the actual alarm date (include the snooze)
    var actualAlarmDate: Date { return fullDate + snoozeDelay }
    
    /// return the date that there won't be any alarm ring after
    var missedDate: Date { return actualAlarmDate + missedDelay }
    
    var snoozed: Bool { return snoozeDelay > 0 && actualAlarmDate.timeIntervalSinceNow > 0 }
    
    var snoozeDelay = 0.seconds
    
    var snoozeDuration = 10.minutes
    var snoozeCount = 0
    
    /// the delay that after that time, it won't ring anymore
    var missedDelay = 30.seconds
    
    var onVacation = true
    
    var edited = false
    
    var shouldWork: Bool {
        if edited {
            return !disabled
        }
        return !disabled && (onVacation || !isInVacation)
    }
    
    init(ruleId:String) {
        self.ruleId = ruleId
    }
}

struct DayTime: CustomStringConvertible {
    var hour: Int
    var minutes: Int 
    var seconds = 0
    var intervals: TimeInterval { return Double(minutes.minutes + hour.hours) }
    
    init(hour: Int, minutes: Int, seconds: Int) {
        self.hour = hour
        self.minutes = minutes
        self.seconds = seconds
    }
    
    init() {
        let d = Date()
        self.init(hour: d.hour, minutes: d.minute, seconds: 0)
    }
    
    var description: String {
        return "\(hour):\(minutes.string2d):\(seconds.string2d)"
    }
}


enum RepeatOption {
    case weekly(repeat:WeeklyRepeat)
    case monthly(repeat:MonthlyRepeat)
    case yearly(repeat:YearlyRepeat)
    case none(date:NoneRepeat)
    
    var text: String {
        switch self {
        case .weekly(let r): return r.text
        case .monthly(let r): return r.text
        case .yearly(let r): return r.text
        case .none(let d): return d.text
        }
    }
    
    func next(from:Date) -> Date? {
        let f = from.start(of: .day)!
        switch self {
        case .none(let r): return r.date > f ? r.date : nil
        case .weekly(let r): return r.next(from: f)
        case .monthly(let r): return r.next(from: f)
        case .yearly(let r): return r.next(from: f)
        }
    }
}





class WeeklyRepeat {
    var days = [Bool](repeatElement(true, count: 7))
    
    
    
    var text: String {
        let daysText = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        
        let res = Array(0..<days.count).compactMap { days[$0] ?  daysText[$0] : nil }
        
        return res.isEmpty ? "None" : res.count == days.count ? "All Days" : res.joined(separator: ", ")
    }
    
    func next(from:Date) -> Date? {
        for i in 0..<days.count {
            let date = from + i.dayComponent
            if days[date.weekDay-1] { return date }
        }
        return nil
    }
    
    func clear() {
        days = [Bool](repeatElement(false, count: 7))
    }
    
    func isEmpty() -> Bool {
        return days.filter { $0 }.count == 0
    }
}


class MonthlyRepeat {
    var days = [Bool](repeatElement(false, count: 31))

    var text: String {
        
        let res = Array(0..<days.count).compactMap { days[$0] ? ($0+1).ordinary : nil }
        
        return res.isEmpty ? "None" : res.count == days.count ? "All Days" : "Every " + res.joined(separator: ", ")
    }
    
    func next(from:Date) -> Date? {
        for i in 0..<days.count {
            let date = from + i.dayComponent
            if days[date.day-1] { return date }
        }
        if (days.filter { $0 }).count > 0 {
            //the month doesn't contain the last days
            return next(from:from + 1.monthComponent)
        }
        return nil
    }
}

class YearlyRepeat {
    var month = 0
    var day = 0
    
    var text: String {
        let d = Date(month: month, day: day)!
        return formatter.string(from: d) + " " + day.ordinary
    }
    
    func next(from:Date) -> Date? {
        let d = Date(month: month, day: day)!
        return d > from ? d : d + 1.yearComponent
    }
    
    private var formatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "MMM"
        return f
    }()

}

class NoneRepeat {
    var date = Date()
    
    var text: String {
        if date.isToday { return "Today" }
        if date.isTomorrow { return "Tomorrow" }
        return formatter.string(from: date)
    }
    
    private var formatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "MMM dd, YYYY"
        return f
    }()

}



extension AlarmRule {
    var noneRepeat: NoneRepeat? {
        if case let RepeatOption.none(date: n) = repeatOption {
            return n
        }
        return nil
    }
    var weeklyRepeat: WeeklyRepeat? {
        if case let RepeatOption.weekly(repeat: w) = repeatOption {
            return w
        }
        return nil
    }
    var monthlyRepeat: MonthlyRepeat? {
        if case let RepeatOption.monthly(repeat: w) = repeatOption {
            return w
        }
        return nil
    }
    var yearlyRepeat: YearlyRepeat? {
        if case let RepeatOption.yearly(repeat: w) = repeatOption {
            return w
        }
        return nil
    }
    
    func getNoneRepeat() -> NoneRepeat {
        if noneRepeat == nil {
            repeatOption = .none(date: NoneRepeat())
        }
        return noneRepeat!
    }
    func getWeeklyRepeat() -> WeeklyRepeat {
        if weeklyRepeat == nil {
            repeatOption = .weekly(repeat: WeeklyRepeat())
        }
        return weeklyRepeat!
    }
    func getMonthlyRepeat() -> MonthlyRepeat {
        if monthlyRepeat == nil {
            repeatOption = .monthly(repeat: MonthlyRepeat())
        }
        return monthlyRepeat!
    }
    func getYearlyRepeat() -> YearlyRepeat {
        if yearlyRepeat == nil {
            repeatOption = .yearly(repeat: YearlyRepeat())
        }
        return yearlyRepeat!
    }
    
}
