//
//  DatePickerViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 12/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class DatePickerViewController: BaseViewController {

    @IBOutlet weak var datePicker : UIDatePicker!
    @IBOutlet weak var lblTitle : UILabel!
    
    private var comp: ((Date)->Void)?
    private var date: Date?
    private var minDate: Date?
    private var mode: UIDatePickerMode = .dateAndTime
    
    static func present(with date:Date? = nil, mode: UIDatePickerMode = .dateAndTime, title: String = "", minDate: Date? = nil, completion: ((Date)->Void)?) {
        let vc = UIStoryboard.main.viewController(id: "DatePickerViewController") as! DatePickerViewController
        vc.comp = completion
        vc.date = date
        vc.mode = mode
        vc.title = title
        vc.minDate = minDate
        UIWindow.presentedViewController?.present(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker.superview?.layer.cornerRadius = 5
        datePicker.date = date ?? Date()
        datePicker.datePickerMode = mode
        datePicker.minimumDate = minDate
        lblTitle.text = title
    }

    
    @IBAction func save() {
        let d = datePicker.date
        comp?(d)
        returnAsPopOrDismiss()
    }
    
}
