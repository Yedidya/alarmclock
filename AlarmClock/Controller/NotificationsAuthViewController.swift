//
//  NotificationsAuthViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 28/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit
import UserNotifications





class NotificationsAuthViewController: PopupMessageViewController {

    static func getAuth() {
        if UserDefaults.authRequested { return }
        presentGetAuth()
    }
    
    
    
    static func validateAuth() {
        guard DataManager.shared.config.testForIosPushAuth else { return }
        if !UserDefaults.authRequested { return }
        requestAuth { (granted) in
            if !granted { presentValidation() }
        }
    }
    
    
    private static func presentGetAuth() {
        present(id: "NotificationsAuthViewController_get")
    }
    
    private static func presentValidation() {
        present(id: "NotificationsAuthViewController_validate")
    }
    
    private static func present(id:String) {
        let vc = UIStoryboard.main.viewController(id: id)
        UIWindow.delayedPresentedViewController { (base) in
            base.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func getAuth() {
        UserDefaults.authRequested = true
        dismiss(animated: true) {
            NotificationsAuthViewController.requestAuth(completion: nil)
//            DispatchQueue.main.asyncAfter(deadline: 1.delay) {
//                NotificationsAuthViewController.validateAuth()
//            }
        }
    }
    
    
    @IBAction func validateAuth() {
        NotificationsAuthViewController.requestAuth { [weak self] (granted) in
            if granted {
                self?.returnAsPopOrDismiss()
            } else {
                self?.blinkMessage(text: "Still missing...")
            }
        }
    }
    
    
    private static func requestAuth(completion:((Bool)->Void)?) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    log.e("notification auth error: \(error.localizedDescription)")
                    completion?(false)
                }
                else if granted {
                    log.i("notification auth granted")
                    completion?(true)
                    UIApplication.shared.registerForRemoteNotifications()
                } else {
                    log.i("notification auth was not granted")
                    completion?(false)
                }
            }
        }
    }
    

}


fileprivate extension UserDefaults {
    static var authRequested: Bool {
        set { UserDefaults.standard.set(newValue, forKey: "NotificationsAuthViewController_authRequested") }
        get { return UserDefaults.standard.bool(forKey: "NotificationsAuthViewController_authRequested") }
    }
}
