//
//  VacationsListViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 30/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class VacationsListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, DataChanedDelegate {

    @IBOutlet weak var table : UITableView!
    
    @IBOutlet weak var lblWeeklyVacation : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedVacation = nil
        table.tableFooterView = UIView()
        updateUI()
    }
    
    private func updateUI() {
        table.reloadData()
        table.isHidden = table.numberOfRows(inSection: 0) == 0
        lblWeeklyVacation.text = DataManager.shared.weeklyVacation.text
    }

    
    
    //////////////////////////////////////////////////////////////
    /////////////////////////// table view ///////////////////////
    //////////////////////////////////////////////////////////////
    // MARK: - table view -
    
    
    private var dataSource: [Vacation] { return DataManager.shared.arrVacations }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! VacationCell
        cell.update(with: dataSource[indexPath.row])
        return cell
        
    }
    
    
    private var selectedVacation: Vacation!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedVacation = dataSource[indexPath.row]
        performSegue(withIdentifier: "vacationVC", sender: nil)
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let action = UITableViewRowAction(style: .destructive, title: "DELETE") { [weak self] (_,_) in
            let v = self!.dataSource[indexPath.row]
            DataManager.shared.deleteVacation(vacation: v)
            self?.updateUI()
        }
        return [action]
    }

    
    ///////////////////////////////////////////////
    //////////////// data changed /////////////////
    ///////////////////////////////////////////////
    // MARK: - data changed -
    
    func dataChanged() {
        DataManager.shared.updateData()
    }

    
    /////////////////////////////////////////
    //////////////// segue //////////////////
    /////////////////////////////////////////
    // MARK: - segue -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? VacationViewController {
            vc.vacation = selectedVacation
        }
        
        if let vc = segue.destination as? DailyRepeatViewController {
            vc.dailyrepeat = DataManager.shared.weeklyVacation
            vc.delegate = self
        }
    }
    
    

}

class VacationCell : UITableViewCell {
    
    private var vacation: Vacation!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblStart : UILabel!
    @IBOutlet weak var lblEnd : UILabel!
    @IBOutlet weak var switchVacation : UISwitch!
    
    func update(with vacation: Vacation) {
        self.vacation = vacation
        lblTitle.text = vacation.title
        lblStart.text = "Start: \(vacation.dateString(date: vacation.start))"
        lblEnd.text = "End: \(vacation.dateString(date: vacation.end))"
        switchVacation.isOn = vacation.enabled
    }
    
    @IBAction func enableChanged() {
        vacation.enabled = switchVacation.isOn
        DataManager.shared.vacationUpdated(vacation: vacation)
    }
    
    
    
    
}

