//
//  MonthlyRepeatViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 03/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class MonthlyRepeatViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var monthrepeat: MonthlyRepeat!
    
    @IBOutlet weak var collection : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var firstLayout = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if firstLayout {
            firstLayout = false
            collection.reloadData()
        }
    }
    
    
    ///////////////////////////////////////////////////////////////////
    /////////////////////////// collection view ///////////////////////
    ///////////////////////////////////////////////////////////////////
    // MARK: - collection view -
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return monthrepeat.days.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = view.window!.frame.width / 7
        return CGSize(width: w , height: w)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "cell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! DayItemCell
        
        cell.update(with: indexPath.row, selected: monthrepeat.days[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        monthrepeat.days[indexPath.row] = !monthrepeat.days[indexPath.row]
        collectionView.reloadItems(at: [indexPath])
    }

}

class DayItemCell: UICollectionViewCell {
    @IBOutlet weak var lblDay : UILabel!
    @IBOutlet weak var viewBG : UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.viewBG.setRoundCorners()
        }
    }
    
    
    func update(with day: Int, selected:Bool) {
        viewBG.backgroundColor = selected ? .blue : .lightGray
        lblDay.textColor = selected ? .white : .black
        lblDay.text = (day+1).string
    }
}
