//
//  AwakeSafetyViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 25/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class AwakeSafetyViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var lblTimer : UILabel!
    @IBOutlet weak var lblQ : UILabel!
    @IBOutlet weak var txtAnswer : UITextField!
    @IBOutlet weak var viewWrapper : UIView!
    
    private var completion: ((Bool)->Void)?
    
    static func show(completion:((Bool)->Void)?) {
        let vc = UIStoryboard.main.viewController(id: "AwakeSafetyViewController") as! AwakeSafetyViewController
        UIWindow.presentedViewController?.present(vc, animated: true, completion: nil)
        vc.completion = completion
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewWrapper.setBaseShadow()
        viewWrapper.setRoundCorners(radius:5)
        lblQ.isHidden = true
        lblTimer.isHidden = true
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtAnswer.becomeFirstResponder()
        restartTest()
    }
    
    

    private var timeLeft = 0
    private var userAnswer = "?"
    private var first = -1
    private var second = -1
    private var question: String { return "\(first) - \(second) = \(userAnswer)" }
    
    
    
    
    
    private func restartTest() {
        let f = Int(arc4random_uniform(10))
        let s = Int(arc4random_uniform(10))
        
        first = f + 10
        second = s + f + 1
        
        userAnswer = "?"
        txtAnswer.text = ""
        lblQ.text = question
        timeLeft = 8
        
        countDown()
        
        lblQ.isHidden = false
        lblTimer.isHidden = false
        
        log.i("new q: \(question)")
    }
    
    
    private func countDown() {
        lblTimer.text = timeLeft.string2d
        if done { return }
        
        if timeLeft == 0 {
            restartTest()
            return
        }
        if timeLeft == 1 {
            viewWrapper.shake()
        }
        timeLeft -= 1
        DispatchQueue.main.asyncAfter(deadline: 1.delay) { [weak self] in
            self?.countDown()
        }
    }
    
    
    private func failed() {
        log.v("failed to answer")
        let arr = ["Are you sure you are awake?!","Again?","Need some help? Try again...","Wake up!! You can do better!"]
        blinkMessage(text: arr.random!)
        viewWrapper.shake()
    }
    
    private var done = false
    private func success() {
        log.v("succeed to answer")
        completion?(true)
        done = true
        blinkMessage(text: "Good Morning 👍")
        DispatchQueue.main.asyncAfter(deadline: 0.5.delay) {
            self.returnAsPopOrDismiss()
        }

    }
    
    ////////////////////////////////////////////
    //////////////// textfield /////////////////
    ////////////////////////////////////////////
    // MARK: - textfield -
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        userAnswer = string.isEmpty ? "?" : string
        lblQ.text = question

        log.v("user answer: \(userAnswer)")
        if let i = Int(string) {
            i == first - second ? success() : failed()
        }

        return false
    }
    
    
}
