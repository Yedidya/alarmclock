//
//  VacationViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 30/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class VacationViewController: BaseViewController {

    var vacation: Vacation!
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblStartDate : UILabel!
    @IBOutlet weak var lblStartTime : UILabel!
    @IBOutlet weak var lblEndDate : UILabel!
    @IBOutlet weak var lblEndTime : UILabel!
    
    @IBOutlet weak var switchYearly : UISwitch!
    
    private var isNew = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if vacation == nil {
            isNew = true
            vacation = Vacation()
        } else {
            vacation = vacation.copy()
        }
        updateUI()
    }

    
    private func updateUI() {
        lblTitle.text = vacation.title
                
        if let start = vacation.start {
            lblStartTime.text = "\(start.hour):\(start.minute.string2d)"
            lblStartDate.text = vacation.dateString(date: vacation.start)
        }
        
        if let end = vacation.end {
            lblEndTime.text = "\(end.hour):\(end.minute.string2d)"
            lblEndDate.text = vacation.dateString(date: vacation.end)
        }
        
        switchYearly.isOn = vacation.yearly
    }
    
    
    @IBAction func titlePressed() {
        TextPickerView.showWithTitle("Vacation Name", text: vacation.title, placeholder: "none") { [weak self] (txt) in
            if let txt = txt {
                self?.vacation.title = txt
                self?.updateUI()
            }
        }
    }

    
    @IBAction func yearlyChanged() {
        vacation.yearly = switchYearly.isOn
        updateUI()
    }
    
    @IBAction func startTimePressed() {
        DatePickerViewController.present(with: vacation.start, mode: .time, title: "Start Time") { [weak self] (date) in
            self?.vacation.start = date
            self?.updateUI()
        }
    }
    
    @IBAction func endTimePressed() {
        DatePickerViewController.present(with: vacation.end, mode: .time, title: "End Time") { [weak self] (date) in
            self?.vacation.end = date
            self?.updateUI()
        }
    }
    
    @IBAction func startDatePressed() {
        DatePickerViewController.present(with: vacation.start, mode: .date, title: "Start Date") { [weak self] (date) in
            self?.vacation.start = self?.vacation.start == nil ? date.start(of: .day) : date
            self?.updateUI()
        }
    }
    
    @IBAction func endDatePressed() {
        DatePickerViewController.present(with: vacation.start, mode: .date, title: "End Date") { [weak self] (date) in
            self?.vacation.end = self?.vacation.end == nil ? date.end(of: .day) : date
            self?.updateUI()
        }
    }
    
    @IBAction func savedPressed() {
        guard vacation.start != nil, vacation.end != nil else {
            UIAlertController.show(title: "Error", message: "You must enter start and end date", buttons: [], cancel: "OK", completion: nil)
            return
        }
        
        if isNew {
            DataManager.shared.addVacation(vacation: vacation)
        } else {
            DataManager.shared.vacationUpdated(vacation: vacation)
        }
        
        returnAsPopOrDismiss()
    }
    

}
