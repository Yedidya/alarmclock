//
//  SettingsTableViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 30/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBOutlet weak var lblVacations : UILabel!
    @IBOutlet weak var lblAppversion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    static var vacationCount: Int {
        return DataManager.shared.arrVacations.count + (!DataManager.shared.weeklyVacation.isEmpty()).int
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblVacations.text = "Vacations (\(SettingsTableViewController.vacationCount))"
    }


    @IBAction func sharePressed() {
        let share = "Hi, You must see this alarm clock app!\nYou can change specific upcoming alarm without change all of them!"
        SettingsUtils.share(text: "\(share)\n\(DataManager.shared.config.iosUrl)")
    }
    
    @IBAction func contactUsPressed() {
        let body = "User:\(DataManager.shared.deviceId) \niOS version: \(UIDevice.current.systemVersion) \nApp version: \(Bundle.appBuild())"
        SettingsUtils.contactUs(to: "yedidya.reiss+alarm_clock@gmail.com", subject: "Upcoming Alarm Clock feedback", body: body)
    }

    
    @IBAction func resetTutorialPressed() {
        DataManager.shared.shouldPresentSafetyTutorial = true
        DataManager.shared.shouldPresentReminderTutorial = true
        blinkMessage(text: "Done 👍")
    }
}
