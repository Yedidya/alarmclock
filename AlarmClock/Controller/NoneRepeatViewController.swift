//
//  NoneRepeatViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 03/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class NoneRepeatViewController: BaseViewController {

    var nonerepeat: NoneRepeat!
    
    @IBOutlet weak var datePicker : UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if nonerepeat == nil {
            nonerepeat = NoneRepeat()
        }
        let now = Date()
        datePicker.minimumDate = now
        datePicker.date = nonerepeat.date
    }
    
    @IBAction func dateChange() {
        nonerepeat.date = datePicker.date
    }

}
