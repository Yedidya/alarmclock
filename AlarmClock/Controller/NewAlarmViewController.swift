//
//  NewAlarmViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 08/07/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit
import Firebase

class NewAlarmViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AlarmRuleDataChangedDelegate {

    var alarmRule: AlarmRule!
    var isSilentReminder = false
    
    @IBOutlet weak var table : UITableView!
    
    private var isNew = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if alarmRule == nil {
            alarmRule = isSilentReminder ? silentReminder : AlarmRule()
            alarmRule.time.hour = (alarmRule.time.hour + 1) % 24
            isNew = true
        } else {
            alarmRule = alarmRule.copy()
        }
        
        Analytics.logEvent(isNew ? "new_alarm" : "edited_alarm", parameters: ["event": "present_screen"])

        table.estimatedRowHeight = 44
        table.rowHeight = UITableViewAutomaticDimension
        table.tableFooterView = UIView()
    }
    
    private var silentReminder: AlarmRule {
        let rule = AlarmRule()
        rule.time = DayTime(hour: 22, minutes: 0, seconds: 0)
        rule.title = "Silent Mode Check"
        rule.isReminder = true
        rule.repeatOption = .weekly(repeat: WeeklyRepeat())
        return rule
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        table.reloadData()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationsAuthViewController.getAuth()
    }
    
    
    @IBAction func savePressed() {
        if isNew {
            DataManager.shared.addRule(rule: alarmRule)
        } else {
            DataManager.shared.ruleUpdated(rule: alarmRule)
        }
        Analytics.logEvent(isNew ? "new_alarm" : "edited_alarm", parameters: ["event": "saved"])
        returnAsPopOrDismiss()
    }
    
    
    ///////////////////////////////////////////////
    //////////////// rule changed /////////////////
    ///////////////////////////////////////////////
    // MARK: - rule changed -
    
    func alarmDataChanged() {
        if let none = alarmRule.noneRepeat, none.date.isToday {
            let time = alarmRule.time
            let d = none.date.dateWith(hour: time.hour, minutes: time.minutes, seconds: time.seconds)!
            if d < Date() {
                none.date = d + 1.dayComponent
            }
        }
        table.reloadData()
    }

    
    //////////////////////////////////////////////////////////////
    /////////////////////////// table view ///////////////////////
    //////////////////////////////////////////////////////////////
    // MARK: - table view -

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alarmRule.isReminder ? 6 : 10
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "\(indexPath.row)"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! AlarmRuleFieldCell
        
        cell.update(with: alarmRule)
        cell.delegate = self
        return cell
        
    }
    

    
    /////////////////////////////////////////
    //////////////// segue //////////////////
    /////////////////////////////////////////
    // MARK: - segue -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? RepeatViewController {
            vc.rule = alarmRule
        }
    }
    

}



