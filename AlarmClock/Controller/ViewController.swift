//
//  ViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 08/07/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table : UITableView!
    @IBOutlet weak var segment : UISegmentedControl!
    
    private var arrRules: [AlarmRule] = []
    private var arrAlarms: [[Alarm]] = [[],[],[]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.estimatedRowHeight = 100
        table.rowHeight = UITableViewAutomaticDimension
        table.clearBottomSeparators()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedRule = nil
        refreshData()
    }
    
    
    private var hasAlarm: Bool { return DataManager.shared.arrAlarmRules.count > 0 }
    private var hadAlarm = DataManager.shared.arrAlarmRules.count > 0
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if !hadAlarm && hasAlarm { //after setting the first alarm
            PopupMessageViewController.presentSilentReminder { [weak self] in
                guard $0 else { return }
                self?.isSilentReminder = true
                log.i("create silent remidner")
                self?.performSegue(withIdentifier: "alarmSegue", sender: nil)
            }
        } else {
            //TODO: tutorial for swipe for delete
        }
        isSilentReminder = false
        
    }
    
    
    func refreshData() {
        
        arrRules = DataManager.shared.arrAlarmRules
        
        let alarms = DataManager.shared.arrAlarms
        let today = alarms.filter { $0.fullDate.isToday }
        let tomorrow = alarms.filter { $0.fullDate.isTomorrow }
        let afterTomorrow = Date().start(of: .day)! + 2.dayComponent
        let next = alarms.filter { $0.fullDate > afterTomorrow }
        
        arrAlarms = [today,tomorrow,next]
        
        table.isHidden = arrRules.count == 0
        table.reloadData()
        startRefreshLoop()
    }
    
    
    var loopCounter = 0
    private func startRefreshLoop() {
        table.reloadData()
        loopCounter += 1
        refreshSnooze()
    }
    
    
    private func refreshSnooze() {
        if state != .alarms { return }
        let lc = loopCounter
        DispatchQueue.main.asyncAfter(deadline: 1.delay) { [weak self] in
            let clc = self?.loopCounter ?? lc + 1
            if lc < clc { return }
            if DataManager.shared.arrAlarms.exist(find: { $0.snoozed }) {
                self?.table.reloadData()
                self?.refreshSnooze()
            }
        }
    }
    
    private enum State : Int {
        case alarms = 0
        case rules = 1
    }
    
    private var state: State {
        return State(rawValue: segment.selectedSegmentIndex)!
    }
    
    
    @IBAction func segmentChanged() {
        startRefreshLoop()
        table.setContentOffset(.zero, animated: true)
    }
    
    //////////////////////////////////////////////////////////////
    /////////////////////////// table view ///////////////////////
    //////////////////////////////////////////////////////////////
    // MARK: - table view -
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return state == .alarms ? arrAlarms[section].count : arrRules.count
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return state == .alarms ? 3 : 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if state == .rules { return nil }
        if arrAlarms[section].count == 0 { return nil }
        if section == 0 { return "Today" }
        if section == 1 { return "Tomorrow" }
        if section == 2 { return "Next Up" }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if state == .alarms {
            let identifier = "alarmcell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! AlarmCell
            
            cell.update(with: arrAlarms[indexPath.section][indexPath.row])
            
            return cell

        } else { //state is rules
            let identifier = "rulecell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! AlarmRuleCell
            
            cell.update(with: arrRules[indexPath.row])
            return cell

        }
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if state == .rules {
            let action = UITableViewRowAction(style: .destructive, title: "DELETE") { [weak self] (_,_) in
                
                let rule = self!.arrRules[indexPath.row]
                DataManager.shared.deleteRule(rule: rule)
                self?.refreshData()
            }
            return [action]
        }
        
        return []
        
    }
    
    
    private var selectedRule : AlarmRule?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if state == .rules {
            log.v("did select, rules state")
            selectedRule = arrRules[indexPath.row]
            performSegue(withIdentifier: "alarmSegue", sender: nil)
        } else { // alarms
            log.v("did select, alarms state")
            let alarm = arrAlarms[indexPath.section][indexPath.row]
                        
            let a = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let op0 = UIAlertAction(title: "Got It", style: .default) { _ in
                alarm.done = true
                alarm.dataChanged()
            }
            
            let op1 = UIAlertAction(title: "Edit", style: .default) { _ in
                log.i("edit alarm: \(alarm.id)")
                DatePickerViewController.present(with: alarm.fullDate) { [weak self] (date) in
                    
                    log.d("edit with date: \(date), alarm : \(alarm.id)")
                    alarm.date = date
                    alarm.time = DayTime(hour: date.hour, minutes: date.minute, seconds: date.second)
                    alarm.dataChanged()
                    self?.refreshData()
                }
                
            }
            
            let postpone = { (_ delay:TimeInterval) in
                log.i("postpone with: \(delay.int.toMinutes.int) min, alarm: \(alarm.id)")
                let past = min(0,alarm.actualAlarmDate.timeIntervalSinceNow)
                alarm.snoozeDelay += (delay + -1 * past)
                alarm.done = false
                alarm.disabled = false
                alarm.dataChanged()
                self.startRefreshLoop()
            }
            
            let op2 = UIAlertAction(title: "Postpone + 5 min", style: .default) { _ in
                postpone(5.minutes)
            }
            
            let op3 = UIAlertAction(title: "Postpone + 10 min", style: .default) { _ in
                postpone(10.minutes)
            }
            
            let op4 = UIAlertAction(title: "Postpone + 30 min", style: .default) { _ in
                postpone(30.minutes)
            }
            
            let op5 = UIAlertAction(title: "Postpone + 1 hour", style: .default) { _ in
                postpone(1.hours)
            }
            
            let op6 = UIAlertAction(title: "Reset", style: .destructive) { _ in
                log.i("reset alarm: \(alarm.id)")
                DataManager.shared.resetAlarm(alarm: alarm)
                self.refreshData()
            }
            
            let op7 = UIAlertAction(title: "Cancel", style: .cancel)
            
            a.addAction(op0)
            a.addAction(op1)
            a.addAction(op2)
            a.addAction(op3)
            a.addAction(op4)
            a.addAction(op5)
            a.addAction(op6)
            a.addAction(op7)
            
            self.present(a, animated: true, completion: nil)
        }
    }
    
    
    /////////////////////////////////////////
    //////////////// segue //////////////////
    /////////////////////////////////////////
    // MARK: - segue -
    
    private var isSilentReminder = false
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nv = segue.destination as? UINavigationController, let vc = nv.viewControllers.first as? NewAlarmViewController {
            vc.alarmRule = selectedRule
            vc.isSilentReminder = isSilentReminder
            hadAlarm = hasAlarm
            
        }
    }
    
    

}



protocol AlarmData {
    var timeString: String { get }
    var title: String { get }
    var disabled: Bool { get set }
    var isDisabled: Bool { get }
    
    func dataChanged()
}



extension AlarmRule: AlarmData {
    func dataChanged() {
        DataManager.shared.ruleUpdated(rule: self)
    }
    
    var timeString: String {
        return "\(time.hour):\(time.minutes.string2d)"
    }
    
    var isDisabled: Bool { return disabled || noneRepeat?.date.end(of: .day)?.inThePast == true }
}



extension Alarm : AlarmData {
    func dataChanged() {
        edited = true
        DataManager.shared.alarmUpdated(alarm: self)
    }
    
    var isDisabled: Bool { return !shouldWork || done || missedDate.inThePast }
    
    var timeString: String {
        return "\(actualAlarmDate.hour):\(actualAlarmDate.minute.string2d)"
    }
}


class AlarmDataCell: UITableViewCell {
    
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    
    @IBOutlet weak var switchDisable : UISwitch!
    
    @IBOutlet var arrLabels : [UILabel]!
    
    private var data: AlarmData!
    
    func _update(with alarmData: AlarmData) {
        data = alarmData
        update()
    }
    
    private func update() {
        lblTime.text = data.timeString
        lblTitle.text = "Title: " + (data.title.isEmpty ? "None" : data.title)
        switchDisable.isOn = !data.isDisabled
        arrLabels.forEach {
            $0.alpha = data.isDisabled ? 0.22 : 1
        }
    }
    
    
    @IBAction func disableChanged() {
        data.disabled = !switchDisable.isOn
        data.dataChanged()
        update()
    }
}

class AlarmRuleCell: AlarmDataCell {

    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblLock : UILabel!
    @IBOutlet weak var lblVacation : UILabel!
    
    func update(with rule:AlarmRule) {
        _update(with: rule)
        
        let title = rule.noneRepeat.exist() ? "Date: " : "Repeat: "
        lblDate.text = title + rule.repeatOption.text
        lblLock.text = rule.awakeSafety ? "🔒" : "🔓"
        lblVacation.text = rule.onVacation ? "On" : "Off"
    }
}

class AlarmCell: AlarmDataCell {
    
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblVacation : UILabel!
    @IBOutlet weak var lblSnooze : UILabel!
    @IBOutlet weak var lblEdited : UILabel!
    
    private var alarm: Alarm!

    private static let formatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "MMM dd, YYYY"
        return f
    }()
    
    
    func update(with alarm:Alarm) {
        _update(with: alarm)
        
        self.alarm = alarm
        
        if alarm.date.isToday {
            lblDate.text = "Today"
        } else if alarm.date.isTomorrow {
            lblDate.text = "Tomorrow"
        } else {
            lblDate.text = AlarmCell.formatter.string(from: alarm.date)
        }
        
        lblVacation.isHidden = !alarm.isInVacation
        
        lblEdited.isHidden = !alarm.edited
        
        lblSnooze.isHidden = !alarm.snoozed
        
        if alarm.snoozed {
            let delay = min(alarm.actualAlarmDate.timeIntervalSinceNow, alarm.snoozeDelay)
            let m = delay.toMinutes.int
            let s = delay - m.minutes
            lblSnooze.text = "+\(m):\(s.int.string2d)"
        }
        
    }
    
    override func disableChanged() {
        super.disableChanged()
        update(with: alarm)
    }
    
    
}
