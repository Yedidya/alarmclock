//
//  PopupMessageViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 01/09/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit


class PopupMessageViewController: BaseViewController {
    
    @IBOutlet weak var viewWrapper : UIView!
    
    var completion: ((Bool)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewWrapper.setRoundCorners(radius: 5)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmPressed() {
        close(accept: true)
    }
    
    @IBAction func declinePressed() {
        close(accept: false)
    }
    
    private func close(accept:Bool) {
        dismiss(animated: true) {
            DispatchQueue.main.asyncAfter(deadline: 0.5.delay) {
                self.completion?(accept)
            }
        }
    }
    
}


extension PopupMessageViewController {
    static func presentSilentReminder(completion:((Bool)->Void)?) {
        let vc = UIStoryboard.main.viewController(id: "silentReminderPopup") as! PopupMessageViewController
        vc.completion = completion
        UIWindow.presentedViewController?.present(vc, animated: true, completion: nil)
    }
}
