//
//  RepeatViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 03/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit

class RepeatViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {


    var rule: AlarmRule!
    
    @IBOutlet weak var table : UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        table.reloadData()
    }
    
    //////////////////////////////////////////////////////////////
    /////////////////////////// table view ///////////////////////
    //////////////////////////////////////////////////////////////
    // MARK: - table view -
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = indexPath.row.string
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RepeatCell
        
        switch indexPath.row {
        case 0: cell.lblTitle.text = rule.noneRepeat?.text ?? "-"
        case 1: cell.lblTitle.text = rule.weeklyRepeat?.text ?? "-"
        case 2: cell.lblTitle.text = rule.monthlyRepeat?.text ?? "-"
        case 4: cell.lblTitle.text = rule.yearlyRepeat?.text ?? "-"
        default: break
        }
        
        return cell
        
    }
    
    
    /////////////////////////////////////////
    //////////////// segue //////////////////
    /////////////////////////////////////////
    // MARK: - segue -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? NoneRepeatViewController {
            vc.nonerepeat = rule.getNoneRepeat()
        }
        if let vc = segue.destination as? DailyRepeatViewController {
            vc.dailyrepeat = rule.getWeeklyRepeat()
        }
        if let vc = segue.destination as? MonthlyRepeatViewController {
            vc.monthrepeat = rule.getMonthlyRepeat()
        }

    }
    
    

}


class RepeatCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel!
}
