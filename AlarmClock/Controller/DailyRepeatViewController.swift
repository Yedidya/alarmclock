//
//  DailyRepeatViewController.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 03/08/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit


protocol DataChanedDelegate: class {
    func dataChanged()
}

class DailyRepeatViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tbl : UITableView!
    
    weak var delegate : DataChanedDelegate?
    
    var dailyrepeat: WeeklyRepeat!
    
    private var dataChanged = false
    private var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if dataChanged {
            delegate?.dataChanged()
        }
    }
    
    //////////////////////////////////////////////////////////////
    /////////////////////////// table view ///////////////////////
    //////////////////////////////////////////////////////////////
    // MARK: - table view -
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dailyrepeat.days.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.textLabel?.text = "Every \(days[indexPath.row])"
        cell.accessoryType = dailyrepeat.days[indexPath.row] ? .checkmark : .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dailyrepeat.days[indexPath.row] = !dailyrepeat.days[indexPath.row]
        tableView.reloadRows(at: [indexPath], with: .fade)
        dataChanged = true
    }
}


