//
//  AppDelegate.swift
//  AlarmClock
//
//  Created by Yedidya Reiss on 08/07/2018.
//  Copyright © 2018 Yedidya Reiss. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging

import ShipBookSDK

public typealias log = ShipBookSDK.Log

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(5.hours)
        
        ShipBook.start(appId:"5b71c80a2d80d51ea6cdbbb7", appKey:"2e5d609074354971bf1fc1dc8930e0fd")
        ShipBook.registerUser(userId: DataManager.shared.deviceId)
        
        log.i("didFinishLaunching")
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        UNUserNotificationCenter.current().delegate = self
        NotificationsAuthViewController.validateAuth()
        DataManager.shared.setupNotificationsActions()
        DataManager.shared.updateData()
        
        ConnectionManager.shared.setup()
        ConnectionManager.shared.updateUserData()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        ConnectionManager.shared.updateUserData()

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        log.i("applicationWillEnterForeground")
        DataManager.shared.updateData()
        ConnectionManager.shared.updateUserData()
        if let vc = UIWindow.presentedViewController as? ViewController {
            vc.refreshData()
        }
        NotificationsAuthViewController.validateAuth()

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        log.i("fetching data on background")
        DataManager.shared.updateData()
        completionHandler(.newData)
    }
    
    
    ////////////////////////////////////////////////
    //////////////// notifications /////////////////
    ////////////////////////////////////////////////
    // MARK: - notifications -
    
    
    //TODO: change to winter clock
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let alarmId = response.notification.request.content.userInfo["alarmId"] as? String {
            if let action = Notification.Action(rawValue: response.actionIdentifier) {
                log.i("Did receive notification with action: \(action), for alarm: \(alarmId)")
                DataManager.shared.handleNotifications(alarmId: alarmId, action: action)
            } else  {
                log.i("Did receive notification without action. alarm: \(alarmId)")
                DataManager.shared.handleNotifications(alarmId: alarmId, action: .stop)
            }
        }
        else {
            log.e("No alarm Id received")
            DataManager.shared.updateData()
        }
        
        completionHandler()
    }

    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        log.i("Apn Token: " + deviceToken.hexString)
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        log.i("User fcm token: " + fcmToken)
        DataManager.shared.fcmToken = fcmToken
    }
}



